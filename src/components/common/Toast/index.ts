// import { createVNode, nextTick, render } from "vue";
import { createApp } from "vue";
import Toast from "./Toast.vue";

interface IToastConfig {
  message: string;
  delay?: number;
}
const createToast = ({
  message = "提示",
  delay = 2000,
}: IToastConfig): void => {
  const toastInstance = createApp(Toast, {
    message,
    delay,
  });
  const mountNode = document.createElement("div");
  document.body.appendChild(mountNode);
  toastInstance.mount(mountNode);
  const timer = setTimeout(() => {
    toastInstance.unmount();
    document.body.removeChild(mountNode);
    clearTimeout(timer);
  }, delay);
};
export default createToast;
