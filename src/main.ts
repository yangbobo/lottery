import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import "@/assets/styles/reset.scss";
const app = createApp(App);
app.config.errorHandler = (err: any) => {
  // 处理错误
  // `info` 是 Vue 特定的错误信息，比如错误所在的生命周期钩子
  const message = err.message;
  console.log(message);
};
app.use(router).mount("#app");
