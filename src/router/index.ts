import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: "/chromo-sphere",
  },
  {
    path: "/chromo-sphere",
    name: "ChromoSphere",
    component: () =>
      import(
        /* webpackChunkName: "ChromoSphere" */ "../views/ChromoSphere.vue"
      ),
  },
  {
    path: "/super-lotto",
    name: "super-lotto",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "SuperLotto" */ "../views/SuperLotto.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
