/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { formatNum } from "@/utils/utils";
import { IBallState } from "@/types";
export function initBall(count: number): IBallState[] {
  const res: IBallState[] = [];
  let obj: IBallState;
  for (let i = 1; i < count; i++) {
    obj = {
      active: false,
      num: formatNum(i),
      id: i > 0 ? i - 1 : i,
    };
    res.push(obj);
  }
  return res;
}
