export interface IRes {
  data: any;
  error_code: number;
  msg: string;
}

export interface IOrderInfoType {
  blue: string;
  red: string;
  total: number;
  count: number;
  type: string;
  number: string;
  moment: number;
}
