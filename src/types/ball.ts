export interface IBallState {
  active: boolean;
  num: string;
  id: number;
}

export interface IBallType {
  amount: number;
  ballType: string;
  leastPcs: number;
}

export interface ICheckList<T> {
  [property: string]: Array<T>;
}
