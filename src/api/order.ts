import request from "@/utils/request";
import { IOrderInfoType } from "@/types";
class OrderApi {
  // 获取所有订单信息
  static fetchORder() {
    return request.get("/getAllORder");
  }
  // 下单接口
  static postOrder(payload: IOrderInfoType) {
    return request.post("/postOrder", {
      ...payload,
    });
  }
}

export default OrderApi;
