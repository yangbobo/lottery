import { IBallState } from "@/types";
/**
 * 补零
 * @param num number
 * @returns string
 */
export function formatNum(num: number): string {
  if (Number(num) < 10) {
    return "0" + num;
  }
  return `${num}`;
}
/**
 *
 * @param total 选中小球个数
 * @param product 计算个数比
 */
export function notesTotal(total: number, product: number): number {
  if (total < 0 || product < 0 || total < product) {
    return 0;
  }
  product = product < total - product ? product : total - product;
  if (product === 0) {
    return 1;
  }
  let item = total;
  for (let i = 2, j = item - 1; i <= product; i++, j--) {
    item = (item * j) / i;
  }
  return item;
}

export function checkBallList(list: IBallState[]): string {
  return list.map((item: IBallState) => item.num).toString();
}
