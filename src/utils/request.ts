import axios from "axios";
import Config from "@/common/config";
import { IRes } from "@/types";

const request = axios.create({
  baseURL: Config.API_URI,
  headers: {
    "Content-Type": "application/json; charset=UTF-8",
  },
});

request.interceptors.response.use((response: any) => {
  const res: IRes = response.data;
  // 当 error_code 不为 0 时
  if (res.error_code) {
    console.log(res.msg);
    throw new Error(res.msg);
  }
  // 当 error_code 为 0 时，继续返回请求
  return response.data;
});

export default request;
