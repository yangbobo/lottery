import { shallowMount } from "@vue/test-utils";
import RuleTips from "@/components/RuleTips.vue";
describe("RuleTips.vue", () => {
  it("renders props.tips when passed", () => {
    const tips = "new message";
    const wrapper = shallowMount(RuleTips as any, {
      props: { tips },
    });
    expect(wrapper.text()).toEqual(tips);
  });
});
