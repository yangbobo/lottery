import { shallowMount } from "@vue/test-utils";
import BallSize from "@/components/BallSize.vue";
import { initBall } from "@/hooks/useInitBall";
describe("BallSize.vue", () => {
  it("当ballType为red时ballList为33个小球初始化红字红边，选中发送选中事件", () => {
    const ballType = "red";
    const amount = 34;
    const ballList = initBall(amount);
    const wrapper = shallowMount(BallSize as any, {
      props: { ballType, ballList },
    });
    const redBallListLength = wrapper.vm.ballList;
    const ballColor = wrapper.vm.ballColor;
    expect(redBallListLength.length).toBe(33);
    expect(ballColor).toBe("ball-mod-wrap-red");
  });
  it("当ballType为blue时ballList为16个并且小球颜色蓝字蓝边", () => {
    const ballType = "blue";
    const amount = 17;
    const ballList = initBall(amount);
    const wrapper = shallowMount(BallSize as any, {
      props: { ballType, ballList },
    });
    const blueBallListLength = wrapper.vm.ballList;
    const ballColor = wrapper.vm.ballColor;
    expect(blueBallListLength.length).toBe(16);
    expect(ballColor).toBe("ball-mod-wrap-blue");
  });
  it("选中小球发射选中事件", async () => {
    const ballType = "red";
    const amount = 34;
    const ballList = initBall(amount);
    const wrapper = shallowMount(BallSize as any, {
      props: { ballType, ballList },
    });
    const selectButton = wrapper.findAll(".ball-mod-wrap-li")[1];
    await selectButton.trigger("click");
    expect(wrapper.emitted("select-right")).toBeTruthy();
    expect(wrapper.emitted("select-right")).toEqual([[1]]);
  });
  it("未传入小球列表时候，默认列表长度为0", async () => {
    const wrapper = shallowMount(BallSize as any);
    expect(wrapper.vm.ballList.length).toBe(0);
  });
});
