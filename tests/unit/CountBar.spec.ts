import { shallowMount } from "@vue/test-utils";
import CountBar from "@/components/CountBar.vue";
describe("CountBar.vue", () => {
  it("当coun为1和total为2时，返回共1注2元", () => {
    const count = 1;
    const total = 2;
    const wrapper = shallowMount(CountBar as any, {
      props: { count, total },
    });
    const countModInfo = wrapper.find(".count-mod-info");
    expect(countModInfo.text()).toEqual("共1注2元");
  });
  it("当点击清空按钮，向外发送清空事件", () => {
    const wrapper = shallowMount(CountBar as any);
    const clearBtn = wrapper.find(".count-mod-clear");
    clearBtn.trigger("click");
    expect(wrapper.emitted("select-clear")).toBeTruthy();
  });
  it("当点击确定按钮，向外发送确定请求事件", () => {
    const wrapper = shallowMount(CountBar as any);
    const clearBtn = wrapper.find(".count-mod-confirm");
    clearBtn.trigger("click");
    expect(wrapper.emitted("select-confirm")).toBeTruthy();
  });
});
