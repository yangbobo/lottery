import { mount } from "@vue/test-utils";
import Dialog from "@/components/common/Dialog.vue";
describe("Dialog.vue", () => {
  const title = "标题";
  const visible = true;
  const wrapper = mount(Dialog as any, {
    propsData: { title, visible },
  });
  it("对传入值的值的检查以及是否正常打开", async () => {
    expect(typeof wrapper.vm.title).toBe("string");
    expect(typeof wrapper.vm.visible).toBe("boolean");
    await wrapper.setProps({ title: "确认下注", visible: true });
    expect(wrapper.vm.title).toBe("确认下注");
    expect(wrapper.vm.visible).toBe(true);
  });
  it("当点击取消按钮，向外发送清空事件", async () => {
    await wrapper.vm.onClose("click");
    expect(wrapper.emitted("dialog-on-close")).toBeTruthy();
  });
  it("当点击确定按钮，向外发送清空事件", async () => {
    await wrapper.vm.onConfirm("click");
    expect(wrapper.emitted("dialog-on-confirm")).toBeTruthy();
  });
});
