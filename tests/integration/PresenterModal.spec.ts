import { mount } from "@vue/test-utils";
import PresenterModal from "@/views/widgets/PresenterModal.vue";
import { IBallState } from "@/types";
beforeEach(() => {
  const mockNoop = () => new Promise(() => {});
  jest.mock("axios", () => ({
    default: mockNoop,
    get: mockNoop,
    post: mockNoop,
    put: mockNoop,
    delete: mockNoop,
    patch: mockNoop,
  }));
});

describe("PresenterModal.vue", () => {
  it(`
  1. 用户在双色球页面，根据规则选择对应的红球和篮球
  2. 会根据规则计算出注数和金额
  3. 点击清空按钮，页面选中小球会清空
  4. 注数和金额会清零
  `, async () => {
    const redState = {
      amount: 34,
      ballType: "red",
      leastPcs: 6,
    };
    const blueState = {
      amount: 17,
      ballType: "blue",
      leastPcs: 1,
    };
    const lotteryType = "chromo-sphere";
    const wrapper = mount(PresenterModal as any, {
      props: { redState, blueState, lotteryType },
    });
    for (let i = 0; i < 6; i++) {
      wrapper.vm.selectRed(i);
      if (i < 2) {
        wrapper.vm.selectblue(i);
      }
    }
    wrapper.vm.allCheckList.redList = wrapper.vm.redBallList.filter(
      (item: IBallState) => item.active
    );
    wrapper.vm.allCheckList.blueList = wrapper.vm.blueBallList.filter(
      (item: IBallState) => item.active
    );
    expect(wrapper.vm.count).toBe(2);
    expect(wrapper.vm.total).toBe(4);
    await wrapper.vm.selectClear();
    expect(wrapper.vm.count).toBe(0);
    expect(wrapper.vm.total).toBe(0);
  });
  it(`
  1. 用户在双色球页面，根据规则选择对应的红球和篮球
  2. 会根据规则计算出注数和金额
  3. 点击确定按钮，页面选中小球
  4. 金额为0不允许下单
  5. 调用下单接口，会进行弹窗
  6. 点击确定按钮，注数和金额会清零
  `, async (done) => {
    const redState = {
      amount: 34,
      ballType: "red",
      leastPcs: 6,
    };
    const blueState = {
      amount: 17,
      ballType: "blue",
      leastPcs: 1,
    };
    const lotteryType = "chromo-sphere";
    const wrapper = mount(PresenterModal as any, {
      props: { redState, blueState, lotteryType },
    });
    await wrapper.vm.selectConfirm();
    expect(wrapper.vm.dialogIsVisible).toBe(false);
    for (let i = 0; i < 6; i++) {
      wrapper.vm.selectRed(i);
      if (i < 2) {
        wrapper.vm.selectblue(i);
      }
    }
    wrapper.vm.allCheckList.redList = wrapper.vm.redBallList.filter(
      (item: IBallState) => item.active
    );
    wrapper.vm.allCheckList.blueList = wrapper.vm.blueBallList.filter(
      (item: IBallState) => item.active
    );
    expect(wrapper.vm.count).toBe(2);
    expect(wrapper.vm.total).toBe(4);
    await wrapper.vm.selectConfirm();
    expect(wrapper.vm.dialogIsVisible).toBe(true);
    await wrapper.vm.hideAndConfirm();
    wrapper.vm.$nextTick(() => {
      expect(wrapper.vm.count).toBe(0);
      expect(wrapper.vm.total).toBe(0);
      done();
    });
  });
});
