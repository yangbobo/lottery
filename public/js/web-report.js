/**
 * 获取浏览器类型
 */
(function () {
  if (window.Performance) {
    return window.Performance;
  }
  function getBrowser() {
    var UserAgent = navigator.userAgent.toLowerCase();
    var browser = null;
    var browserArray = {
      IE: window.ActiveXObject || "ActiveXObject" in window, // IE
      Chrome:
        UserAgent.indexOf("chrome") > -1 && UserAgent.indexOf("safari") > -1, // Chrome浏览器
      Firefox: UserAgent.indexOf("firefox") > -1, // 火狐浏览器
      Opera: UserAgent.indexOf("opera") > -1, // Opera浏览器
      Safari:
        UserAgent.indexOf("safari") > -1 && UserAgent.indexOf("chrome") == -1, // safari浏览器
      Edge: UserAgent.indexOf("edge") > -1, // Edge浏览器
      QQBrowser: /qqbrowser/.test(UserAgent), // qq浏览器
      WeixinBrowser: /MicroMessenger/i.test(UserAgent), // 微信浏览器
    };
    for (var i in browserArray) {
      if (browserArray[i]) {
        browser = i;
      }
    }
    return browser;
  }

  /**
   获取设备信息
  */
  function getDevices() {
    const ua = navigator.userAgent.toLowerCase();
    const platform = {};
    const MAP_EXP = {
      Weixin: /micromessenger/i,
      Mac: /(mac os x)\s+([\w_]+)/,
      Windows: /(windows nt)\s+([\w.]+)/,
      Ios: /(i(?:pad|phone|pod))(?:.*)cpu(?: i(?:pad|phone|pod))? os (\d+(?:[.|_]\d+){1,})/,
      Android: /(android)\s+([\d.]+)/,
      Ipad: /(ipad).*os\s([\d_]+)/,
      Iphone: /(iphone\sos)\s([\d_]+)/,
    };
    for (let key in MAP_EXP) {
      const uaMatch = ua.match(MAP_EXP[key]);
      platform[`is${key}`] = !!uaMatch;
      if (!!uaMatch && !platform.version) {
        platform.version =
          key === "Ios" ? uaMatch[2].replace(/_/g, ".") : uaMatch[2];
      }
    }
  }
  /**
   *
   * @returns 获得系统信息
   */
  function getSystemVersion() {
    var ua = window.navigator.userAgent;
    if (ua.indexOf("CPU iPhone OS ") >= 0) {
      return ua.substring(
        ua.indexOf("CPU iPhone OS ") + 14,
        ua.indexOf(" like Mac OS X")
      );
    } else if (ua.indexOf("Android ") >= 0) {
      return ua.substr(ua.indexOf("Android ") + 8, 3);
    } else {
      return "other";
    }
  }
  // 错误默认信息
  const errordefo = {
    time: "",
    n: "js",
    msg: "",
    data: {},
  };
  const conf = {
    // 错误列表
    errorList: [],
    // 来自域名
    preUrl:
      document.referrer && document.referrer !== location.href
        ? document.referrer
        : "",
    // 当前页面
    page: "",
  };
  // 配置参数合并传入
  let opt = {
    domain: "http://localhost/api",
    // 是否上报错误信息
    isError: true,
    // 是否上报页面性能数据
    isPage: true,
    // 脚本延迟上报时间
    outtime: 600,
  };

  Performance.addError = function (err) {
    err = {
      method: "GET",
      msg: err.msg,
      n: "js",
      data: {
        col: err.col,
        line: err.line,
        resourceUrl: err.resourceUrl,
      },
    };
    conf.errorList.push(err);
  };

  function Performance(option) {
    try {
      opt = Object.assign(opt, option);

      // error上报
      if (opt.isError) {
        _error();
      }
    } catch (error) {
      console.log(error);
    }
  }

  function _error() {
    // img,script,css,jsonp, 当资源（如img或script）加载失败，
    // 加载资源的元素会触发一个Event接口的error事件，并执行该元素上的onerror()处理函数。
    // 这些error事件不会向上冒泡到window，但可以在捕获阶段被捕获
    window.addEventListener(
      "error",
      function (e) {
        let defaults = Object.assign({}, errordefo);
        defaults.n = "resource";
        defaults.t = new Date().getTime();
        defaults.msg = e.target.localName + " is load error";
        defaults.method = "GET";
        defaults.data = {
          target: e.target.localName,
          type: e.type,
          resourceUrl: e.target.href || e.target.currentSrc,
        };
        if (e.target != window) conf.errorList.push(defaults);
      },
      true
    );
    // 当有js运行时错误触发时，window会触发error事件
    window.onerror = function (msg, _url, line, col, error) {
      let defaults = Object.assign({}, errordefo);
      setTimeout(function () {
        col = col || (window.event && window.event.errorCharacter) || 0;
        defaults.msg = error && error.stack ? error.stack.toString() : msg;
        defaults.data = {
          resourceUrl: _url,
          line: line,
          col: col,
        };
        defaults.t = new Date().getTime();
        conf.errorList.push(defaults);
        // 上报错误信息
        reportData();
      }, 0);
    };
    window.addEventListener("unhandledrejection", function (e) {
      const error = e && e.reason;
      const message = error.message || "";
      const stack = error.stack || "";
      let resourceUrl, col, line;
      let errs = stack.match(/\(.+?\)/);
      if (errs && errs.length) errs = errs[0];
      errs = errs.replace(/\w.+[js|html]/g, ($1) => {
        resourceUrl = $1;
        return "";
      });
      errs = errs.split(":");
      if (errs && errs.length > 1) line = parseInt(errs[1] || 0);
      col = parseInt(errs[2] || 0);
      let defaults = Object.assign({}, errordefo);
      defaults.msg = message;
      defaults.t = new Date().getTime();
      defaults.data = {
        resourceUrl: resourceUrl,
        line: col,
        col: line,
      };
      conf.errorList.push(defaults);
      try {
        if (!e || !e.reason) {
          return;
        }
        //判断当前被捕获的异常url，是否是异常处理url，防止死循环
        if (e.reason.config && e.reason.config.url) {
          this.url = e.reason.config.url;
        }
        this.msg = e.reason;
        this.recordError();
      } catch (error) {
        console.log(error);
      }
      reportData();
    });
  }

  function reportData(type = 1) {
    setTimeout(() => {
      //错误上报 type 1, 页面曝光 type 2
      let result = {
        ua: window.navigator.userAgent,
        browser: getBrowser(),
        os: getDevices(),
        osVersion: getSystemVersion(),
        time: new Date().getTime(),
        type: type,
        url: location.href,
      };
      // 页面内错误信息上报;
      result = Object.assign(result, {
        errorList: JSON.stringify(conf.errorList),
      });
      CreateIMageReport(opt.url, result);
    });
  }

  function CreateIMageReport(url, err_info) {
    if (!url || !err_info) {
      return;
    }
    try {
      const image = document.createElement("img");
      // 使用base64编码，避免出现特殊字符导致的错误
      let t = +new Date();
      image.src = `${url}?t=${t}&info=${formatParams(err_info)}`;
    } catch (e) {
      console.log(e);
    }
  }
  // 格式化参数
  function formatParams(data) {
    let arr = [];
    for (let name in data) {
      arr.push(encodeURIComponent(name) + "=" + encodeURIComponent(data[name]));
    }
    return arr.join("&");
  }
  window.Performance = Performance;
})();
