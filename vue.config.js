/* eslint-disable @typescript-eslint/no-var-requires */
const webpack = require("webpack");
const CompressionPlugin = require("compression-webpack-plugin");
const AddAssetHtmlPlugin = require("add-asset-html-webpack-plugin");
const WebpackBundleAnalyzer = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;
const path = require("path");
const resolve = (url) => path.resolve(__dirname, url);
// 仅在生产环境下使用
const IS_PROD = process.env.NODE_ENV === "production";
const productionGzipExtensions = ["js", "css"];
module.exports = {
  configureWebpack(config) {
    let plugins = [
      new CompressionPlugin({
        filename: "[path].gz[query]",
        algorithm: "gzip",
        test: new RegExp("\\.(" + productionGzipExtensions.join("|") + ")$"),
        threshold: 10240,
        minRatio: 0.8,
      }),
    ];
    if (IS_PROD) {
      config.optimization = {
        splitChunks: {
          cacheGroups: {
            common: {
              name: "chunk-common",
              chunks: "initial",
              minChunks: 2,
              maxInitialRequests: 5,
              minSize: 0,
              priority: 1,
              reuseExistingChunk: true,
              enforce: true,
            },
            vendors: {
              name: "chunk-vendors",
              test: /[\\/]node_modules[\\/]/,
              chunks: "initial",
              priority: 2,
              reuseExistingChunk: true,
              enforce: true,
            },
          },
        },
      };
      plugins = plugins.concat([
        new webpack.DllReferencePlugin({
          context: process.cwd(),
          manifest: require("./public/vendor/vendor-manifest.json"),
        }),
        // 将 dll 注入到 生成的 html 模板中
        new AddAssetHtmlPlugin({
          // dll文件位置
          filepath: resolve("./public/vendor/*.js"),
          // dll 引用路径
          publicPath: "./vendor",
          // dll最终输出的目录
          outputPath: "./vendor",
        }),
        // new WebpackBundleAnalyzer(),
      ]);
    }
    return {
      plugins,
    };
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/assets/styles/mixin.scss";`,
      },
    },
  },
};
