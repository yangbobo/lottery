# lottery
[彩票项目远程连接](https://gitlab.com/yangbobo/lottery.git)

> 本项目基于`Vue3+typescript`搭建，提供了后台服务提供下单接口，`koa+mysql`实现

### 提交规范
> 基于`Angular` 规范，这是目前使用最广的写法，比较合理和系统化，并且有配套的工具。

- 在使用`git add .` 之后,使用`git cz -a`命令可以根据提示，生成自动化的 `commit message` 
- 在使用`git cz`之前需要全局 `commitizen` 为我们提供一些 `cli` 命令
``` js
npm install commitizen -g
```
###  增加前端项目自动化部署 
- 基于`docker + gitlab + jenkins`，来实现前端自动化部署流程，具体实现效果为开发人员在本地开发，开发push提交代码到指定分支，自动触发jenkins进行持续集成和自动化部署
- 目前提交到远程会触发push钩子，自动触发构建任务,可看到jenkins控制台具体输出信息
``` js
SSH: Connecting from host [37181e29eafc]
SSH: Connecting with configuration [admin] ...
SSH: EXEC: completed after 209 ms
SSH: Disconnecting configuration [admin] ...
SSH: Transferred 1 file(s)
Build step 'Send files or execute commands over SSH' changed build result to SUCCESS
Finished: SUCCESS
```
``` js
服务器ip: 45.76.42.59
username: root
password: 6@Bzzy]-6Ro@(BY]

jenkins
账号: admin
密码：029ec5c43ea84759897b4bf254ab069d
```
### 错误上报
- 上报脚本位置`public/js/web-report.js`，通过监听`unhandledrejection`,`window.addEventListener(("error"),()=>(){})`,`window.error`，来捕获错误脚本，可大致获取到堆栈信息，设备信息，发生事件等。
- 未实现页面加载时间，白屏时间，pvuv等

### webpack优化
在`vue.config.js`中实现`dll`拆分`bundles`，对包进行`gzip`压缩，配置`splitChunks`拆分`chunks`

### 服务端后台
- 实现了`koa+mysql+typescript`的简单后台接口，在文件的`server`中，还未发布到线上
- 实现接口有彩票下单接口，以及查询所有下单彩票

### docker容器部署
- 在服务端实现docker容器部署，node暴露5000端口，
- mysql 3306接口

[前端页面访问连接](http://45.76.42.59),默认80端口

- 后端接口直接访问
```js
查询所有彩票订单
http://45.76.42.59:5000/api/getAllORder
返回数据格式
{"error_code":0,"data":[{"id":1,"red":"18,19,20,25,26,27","blue":"04,05","number":"18,19,20,25,26,27,04,05","type":"super-lotto","moment":"1620363790319","count":6,"total":12}],"msg":""}

选中彩票下单
http://45.76.42.59:5000/api/postOrder
传输数据字段
blue: "04,05"
count: 6
moment: 1620363790319
number: "18,19,20,25,26,27,04,05"
red: "18,19,20,25,26,27"
total: 12
type: "super-lotto"
```

### 单元测试
- 对components文件下的组件进行单元测试，对视图逻辑操作进行了集成测试
![avatar](./doc/img/test-coverage.png)
