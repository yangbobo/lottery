# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.5](https://gitlab.com/yangbobo/lottery/compare/v0.1.4...v0.1.5) (2021-05-09)


### Features

* **tests:** 增加了单元测试 ([1fc5e54](https://gitlab.com/yangbobo/lottery/commit/1fc5e54279533bd3c87320f3ed86f3e6a5e81444))


### Bug Fixes

* 优化toast调用方式 ([187ec01](https://gitlab.com/yangbobo/lottery/commit/187ec01d36c213fc8b9765b0221f79383c69504e))

### [0.1.4](https://gitlab.com/yangbobo/lottery/compare/v0.1.3...v0.1.4) (2021-05-07)


### Features

* **config:** 增加docker容器部署 ([8897940](https://gitlab.com/yangbobo/lottery/commit/8897940cedde2e6aa3ef182f0a3089d46f12cc11))

### [0.1.3](https://gitlab.com/yangbobo/lottery/compare/v0.1.2...v0.1.3) (2021-05-06)


### Features

* **src:** 增加后台服务和错误上报 ([ae8b48a](https://gitlab.com/yangbobo/lottery/commit/ae8b48a3ef30492289de28a7e88ce470b6b4d2eb))
* **vue.conifg.js:** 优化打包文件和错误上报 ([352f3d6](https://gitlab.com/yangbobo/lottery/commit/352f3d6ab02f794d6c52c16222f4cb42d87f3bb1))

### [0.1.2](https://gitlab.com/yangbobo/lottery/compare/v0.1.1...v0.1.2) (2021-05-05)


### Features

* **server:** 增加后台服务下单接口 ([9b3c6fe](https://gitlab.com/yangbobo/lottery/commit/9b3c6feeb97b69a2a99f12ec2668d9470a891e6b))

### 0.1.1 (2021-05-04)


### Features

* **src:** 做了模块逻辑优化，抽离视图层 ([9034ce4](https://gitlab.com/yangbobo/lottery/commit/9034ce4b51bb94c914676a8e6b6c3e0780a91878))


### Bug Fixes

* **readme.md:** 增加文档说明 ([55a7fda](https://gitlab.com/yangbobo/lottery/commit/55a7fda418be9cbe59e28b7dc62cd6ae2200d987))
