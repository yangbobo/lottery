// import { createTable } from "./mysql";
const CREATE_TABLE_ORDER = `
  CREATE TABLE IF NOT EXISTS lottery_order(
    id INT NOT NULL AUTO_INCREMENT,
    red VARCHAR(200) NOT NULL COMMENT '红球',
    blue VARCHAR(200) NOT NULL COMMENT '蓝球',
    number VARCHAR(200) NOT NULL COMMENT '选中号码',
    type VARCHAR(20) NOT NULL COMMENT '抽奖类型',
    moment VARCHAR(40) NOT NULL COMMENT '下单时间',
    count int(11) NOT NULL DEFAULT 0 COMMENT '注数',
    total int(11) NOT NULL DEFAULT 0 COMMENT '金额',
    PRIMARY KEY(id)
  );
`;
export { CREATE_TABLE_ORDER };
