import mysql from "mysql";
// import { CREATE_TABLE_ORDER } from "./db";
import { MysqlConfig } from "../config";
const pool = mysql.createPool({
  host: MysqlConfig.HOST,
  user: MysqlConfig.USERNAME,
  password: MysqlConfig.PASSWORD,
  database: MysqlConfig.DATABASE,
  port: MysqlConfig.PORT,
});

const query = function (sql: string, values?: Array<any>): Promise<any> {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (err: any, connection) {
      if (err) {
        resolve(err);
      } else {
        connection.query(sql, values, (err: any, result) => {
          console.log("mysql connncted success!", err, result);
          if (err) {
            reject(err);
          } else {
            const results = JSON.parse(JSON.stringify(result));
            resolve(results);
          }
          connection.release();
        });
      }
    });
  });
};

// 创建表
const createTable = (sql: string) => {
  return query(sql, []);
};

// 插入一条数据
const insertData = (tableName: string, value: any) => {
  const _sql = "INSERT INTO ?? SET ?";
  return query(_sql, [tableName, value]);
};
//
// 查找一条数据
const findDataById = (table: string, key: string, value: number) => {
  console.log(table, key, value);
  const _sql = `SELECT * FROM ?? WHERE ${key} = ?`;
  return query(_sql, [table, value]);
};
// 查询所有数据
const findAllOrder = () => {
  const _sql = "SELECT * FROM lottery_order";
  return query(_sql);
};
export { createTable, query, insertData, findDataById, findAllOrder };
