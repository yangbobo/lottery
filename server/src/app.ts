import Koa from "koa";
import cors from "koa2-cors";
import bodyParser from "koa-bodyparser";
import AllRouters from "./routes/index";
import { Config } from "./config";
import { CREATE_TABLE_ORDER } from "./lib/db";
import { createTable } from "./lib/mysql";
const server = new Koa();

server
  .use(cors())
  .use(bodyParser())
  .use(AllRouters.routes())
  .use(AllRouters.allowedMethods());
createTable(CREATE_TABLE_ORDER);
server.listen(Config.PORT);
console.log(`the server is start at port ${Config.PORT}`);
