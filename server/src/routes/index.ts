import { Context } from "koa";
import KoaRouter from "koa-router";
import Response from "../utils/response";
import OrderService from "../services/order";
import { Config } from "../config";
import { StatusCode } from "../utils/enum";
const orderRouter = new KoaRouter({
  prefix: Config.APIPATH,
});

orderRouter.post("/postOrder", async (ctx: Context) => {
  const payload = ctx.request.body;
  console.log("payload", payload);
  // const { blue, red, total, count, type, number } = payload;
  try {
    const data = await OrderService.postOrder(payload);
    if (data) {
      Response({
        ctx,
        statusCode: StatusCode.OK,
        data,
      });
    }
  } catch (error) {
    Response({
      ctx,
      errorCode: 1,
      msg: error.message,
    });
  }
});

orderRouter.get("/getAllORder", async (ctx: Context) => {
  try {
    const data = await OrderService.getAllOrder();
    if (data) {
      Response({
        ctx,
        statusCode: StatusCode.OK,
        data,
      });
    }
  } catch (error) {
    console.log(error);
    Response({
      ctx,
      errorCode: 1,
      msg: error.message,
    });
  }
});
export default orderRouter;
