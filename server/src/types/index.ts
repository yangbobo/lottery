import { Context } from "koa";
export interface IResInfo {
  ctx: Context;
  statusCode?: number;
  data?: any;
  errorCode?: number;
  msg?: string;
}

export interface IOrderInfoType {
  blue: string;
  red: string;
  total: number;
  count: number;
  type: string;
  number: string;
}
