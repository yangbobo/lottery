import { Context } from "koa";
import { IOrderInfoType } from "../types/index";
import { insertData, findAllOrder } from "../lib/mysql";

export default class OrderService {
  public static async getAllOrder() {
    try {
      const result = await findAllOrder();
      console.log("orderresult", result);
      return result;
    } catch (error) {
      throw new Error(error);
    }
  }
  public static async postOrder(payload: IOrderInfoType) {
    console.log(payload, "payload");
    try {
      const result = await insertData("lottery_order", payload);
      console.log("model", result);
      return result;
    } catch (error) {
      throw new Error(error);
    }
  }
}
