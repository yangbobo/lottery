export enum StatusCode {
  /**
   * 成功
   */
  OK = 200,
  /**
   * 服务端错误
   */
  Accepted = 500,
  /**
   * Notfound
   */
  NoContent = 404,
  /**
   * 创建成功
   */
  Created = 201,
}
