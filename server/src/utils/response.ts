/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { StatusCode } from "./enum";
import { IResInfo } from "../types";
const Response = (params: IResInfo) => {
  params.ctx.status = params.statusCode! || StatusCode.OK;
  params.ctx.body = {
    error_code: params.errorCode || 0,
    data: params.data || null,
    msg: params.msg || "",
  };
};

export default Response;
