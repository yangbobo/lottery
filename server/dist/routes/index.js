"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_router_1 = __importDefault(require("koa-router"));
const response_1 = __importDefault(require("../utils/response"));
const order_1 = __importDefault(require("../services/order"));
const config_1 = require("../config");
const enum_1 = require("../utils/enum");
const orderRouter = new koa_router_1.default({
    prefix: config_1.Config.APIPATH,
});
orderRouter.post("/postOrder", (ctx) => __awaiter(void 0, void 0, void 0, function* () {
    const payload = ctx.request.body;
    console.log("payload", payload);
    // const { blue, red, total, count, type, number } = payload;
    try {
        const data = yield order_1.default.postOrder(payload);
        if (data) {
            response_1.default({
                ctx,
                statusCode: enum_1.StatusCode.OK,
                data,
            });
        }
    }
    catch (error) {
        response_1.default({
            ctx,
            errorCode: 1,
            msg: error.message,
        });
    }
}));
orderRouter.get("/getAllORder", (ctx) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield order_1.default.getAllOrder();
        if (data) {
            response_1.default({
                ctx,
                statusCode: enum_1.StatusCode.OK,
                data,
            });
        }
    }
    catch (error) {
        console.log(error);
        response_1.default({
            ctx,
            errorCode: 1,
            msg: error.message,
        });
    }
}));
exports.default = orderRouter;
//# sourceMappingURL=index.js.map