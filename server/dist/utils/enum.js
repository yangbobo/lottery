"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatusCode = void 0;
var StatusCode;
(function (StatusCode) {
    /**
     * 成功
     */
    StatusCode[StatusCode["OK"] = 200] = "OK";
    /**
     * 服务端错误
     */
    StatusCode[StatusCode["Accepted"] = 500] = "Accepted";
    /**
     * Notfound
     */
    StatusCode[StatusCode["NoContent"] = 404] = "NoContent";
    /**
     * 创建成功
     */
    StatusCode[StatusCode["Created"] = 201] = "Created";
})(StatusCode = exports.StatusCode || (exports.StatusCode = {}));
//# sourceMappingURL=enum.js.map