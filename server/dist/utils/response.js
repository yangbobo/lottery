"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/no-non-null-assertion */
const enum_1 = require("./enum");
const Response = (params) => {
    params.ctx.status = params.statusCode || enum_1.StatusCode.OK;
    params.ctx.body = {
        error_code: params.errorCode || 0,
        data: params.data || null,
        msg: params.msg || "",
    };
};
exports.default = Response;
//# sourceMappingURL=response.js.map