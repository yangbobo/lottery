"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findAllOrder = exports.findDataById = exports.insertData = exports.query = exports.createTable = void 0;
const mysql_1 = __importDefault(require("mysql"));
// import { CREATE_TABLE_ORDER } from "./db";
const config_1 = require("../config");
const pool = mysql_1.default.createPool({
    host: config_1.MysqlConfig.HOST,
    user: config_1.MysqlConfig.USERNAME,
    password: config_1.MysqlConfig.PASSWORD,
    database: config_1.MysqlConfig.DATABASE,
    port: config_1.MysqlConfig.PORT,
});
const query = function (sql, values) {
    return new Promise((resolve, reject) => {
        pool.getConnection(function (err, connection) {
            if (err) {
                resolve(err);
            }
            else {
                connection.query(sql, values, (err, result) => {
                    console.log("mysql connncted success!", err, result);
                    if (err) {
                        reject(err);
                    }
                    else {
                        const results = JSON.parse(JSON.stringify(result));
                        resolve(results);
                    }
                    connection.release();
                });
            }
        });
    });
};
exports.query = query;
// 创建表
const createTable = (sql) => {
    return query(sql, []);
};
exports.createTable = createTable;
// 插入一条数据
const insertData = (tableName, value) => {
    const _sql = "INSERT INTO ?? SET ?";
    return query(_sql, [tableName, value]);
};
exports.insertData = insertData;
//
// 查找一条数据
const findDataById = (table, key, value) => {
    console.log(table, key, value);
    const _sql = `SELECT * FROM ?? WHERE ${key} = ?`;
    return query(_sql, [table, value]);
};
exports.findDataById = findDataById;
// 查询所有数据
const findAllOrder = () => {
    const _sql = "SELECT * FROM lottery_order";
    return query(_sql);
};
exports.findAllOrder = findAllOrder;
//# sourceMappingURL=mysql.js.map