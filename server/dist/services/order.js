"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql_1 = require("../lib/mysql");
class OrderService {
    static getAllOrder() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield mysql_1.findAllOrder();
                console.log("orderresult", result);
                return result;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    static postOrder(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(payload, "payload");
            try {
                const result = yield mysql_1.insertData("lottery_order", payload);
                console.log("model", result);
                return result;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
}
exports.default = OrderService;
//# sourceMappingURL=order.js.map