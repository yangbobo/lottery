"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_1 = __importDefault(require("koa"));
const koa2_cors_1 = __importDefault(require("koa2-cors"));
const koa_bodyparser_1 = __importDefault(require("koa-bodyparser"));
const index_1 = __importDefault(require("./routes/index"));
const config_1 = require("./config");
const db_1 = require("./lib/db");
const mysql_1 = require("./lib/mysql");
const server = new koa_1.default();
server
    .use(koa2_cors_1.default())
    .use(koa_bodyparser_1.default())
    .use(index_1.default.routes())
    .use(index_1.default.allowedMethods());
mysql_1.createTable(db_1.CREATE_TABLE_ORDER);
server.listen(config_1.Config.PORT);
console.log(`the server is start at port ${config_1.Config.PORT}`);
//# sourceMappingURL=app.js.map